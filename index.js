﻿/**
 * Created by Adrian on 2016-03-18.
 */

var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    mainRoutes = require('./routes/main'),
    app = express(),
    port = 30000;

var dbName = 'tagmeetDB';
var connectionString = 'mongodb://localhost:27017/' + dbName;

mongoose.connect(connectionString);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', mainRoutes);

var server = app.listen(port, function () {
    console.log('Express server listening on port ' + server.address().port);
});



