/**
 * Created by Adrian on 2016-03-18.
 */
var TagModel = require("../models/tag.js");
var UserModel = require("../models/user.js");
var PlaceModel = require("../models/place.js");
var express = require('express'),
    router = express.Router();
    MainController = require("../controllers/mainController");
var fs = require('fs');

// DO ZROBIENIA
router.route('/placesByCoordinates')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        return res.status(500).send({
            success: true, places: [{placeName: "WMI UAM", placeId: "122", coordinates: [52.466593 , 16.92692]},
                {placeName: "Wydzial Biologii UAM", placeId: "123", coordinates: [52.467109 , 16.924787]}]
        });
    });

// DZIALA
router.route('/tagList')
    .get(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.getTagList(function (response) {
            return res.status(500).send(response);
        });
    });

// DZIALA
router.route('/addTag')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.addTag(req.body.tag, function (response) {
            return res.status(500).send(response);
        });
    });

// DO ZROBIENIA
router.route('/logInPlace')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.getUser(req.body.login, function (response) {
            if(response.success) {
                mainController.logInPlace(response.user, req.body.placeName, req.body.placeId, function (response2) {
                    return res.status(500).send(response2);
                });
            } else {
                return res.status(500).send(response);
            }
        });
    });


// DZIALA
router.route('/uploadImage/:login')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        console.log(req.files.images.originalFilename);
        console.log(req.files.images.path);
        console.log(req.path.login);
        mainController.uploadImage(req.files.images.originalFilename, req.files.images.path, req.path.login, function (response) {
            return res.status(500).send(response);
        });
    });

// DZIALA
router.route('/register')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.registerUser(req.body.login, req.body.image, req.body.tags, function (response) {
            return res.status(500).send(response);
        });
    });

// DZIALA
router.route('/images/:login')
    .get(function (req, res) {
        var login = req.params.login;
        var dirname = "images/";
        var img = fs.readFileSync(dirname + "" + login);
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
        return res.status(500).send({
            success: true
        });
    });

// DO ZROBIENIA
router.route('/placesByTags')
    .post(function (req, res) {
        return res.status(500).send({
            success: true, places: [{placeName: "Wydzial Biologii UAM", placeId: "123", coordinates: [52.467109 , 16.924787],
                users: [{login: "SmutnaZaba.png", image: "https://data1.cupsell.pl/upload/generator/52137/640x420/1483543_print-trimmed-1.png?resize=max_sizes&key=55f9a22768eed085006592c1174c0235", date: "19-03.2015 12:01" },
                    {login: "Koczi", image: "https://pbs.twimg.com/profile_images/590194554051100672/o3rM1q2b.jpg", date: "19-03.2015 13:11" },
                    {login: "DariaMaria", image: "https://pbs.twimg.com/profile_images/378800000757903870/25fe7e4f6690007170d2b0f8f7304384_400x400.png", date: "19-03.2015 15:12" },
                    {login: "Mirobola", image: "http://2.bp.blogspot.com/-iS9NijD2clA/TndWrnf2cxI/AAAAAAAAAJ8/U9h7ZpLwQek/s200/60156-ugly_girl.jpg", date: "19-03.2015 11:11" },
                    {login: "Leszke", image: "http://cdn1.se.smcloud.net/t/photos/t/169054/lech-walesa-jak-swinia-solidarnosc_19548590.jpg", date: "18-03.2015 21:37" }
                ]},
                {placeName: "WMI UAM", placeId: "122", coordinates: [52.466593 , 16.92692],
                    users: [{login: "AnnaKiss", image: "http://www.hairstyle24.pl/wp-content/uploads/2012/08/anne-hathaway-dlugie-wlosy.jpg", date: "19-03.2015 13:01" },
                        {login: "DzikiDzik", image: "https://pbs.twimg.com/profile_images/378800000840224937/ad1079aa1bd374a446b41ceef4cb1fa5_400x400.jpeg", date: "19-03.2015 12:05" },
                        {login: "SerMasterka", image: "https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/12080542_900862206670380_1879959255_n.jpg?ig_cache_key=MTA4ODIzMzc4MjUwMDYyMDk2Nw%3D%3D.2", date: "19-03.2015 13:41" }
                ]}]
        });
    });


// DZIALA
router.route('/isLoginAvailable')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.isLoginAvailable(req.body.login, function (response) {
            return res.status(500).send(response);
        });
    });
// DZIALA
router.route('/getUserTags')
    .post(function (req, res) {
        var mainController = new MainController(UserModel, PlaceModel, TagModel);
        mainController.getUserTags(req.body.login, function (response) {
            return res.status(500).send(response);
        });
    });


module.exports = router;