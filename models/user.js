/**
 * Created by Adrian on 2016-03-19.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    login: String,
    image: String,
    tags: [String]
});

var User = mongoose.model('User', UserSchema);

module.exports = User;
