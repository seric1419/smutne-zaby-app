/**
 * Created by Adrian on 2016-03-19.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlaceSchema = new Schema({
    name: String,
    coordinates: [String, String],
    users: []
});

var Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;
