/**
 * Created by Adrian on 2016-03-19.
 */

var fs = require('fs');

var MainController = function (userModel, placeModel, tagModel)
{
    this.userModel = userModel;
    this.placeModel = placeModel;
    this.tagModel = tagModel;
};

MainController.prototype.addTag = function (tag, callback) {
    var me = this;
    me.tagModel.findOne({name: tag}, function (err, newtag) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        if(newtag) {
            return callback({success: false, msg: "Juz istnieje"});
        } else {
            var newtag = new me.tagModel({name: tag});
            newtag.save(function (err) {
                if (err) {
                    return callback({success: false, msg: "Blad bazy danych"});
                }
                return callback({success: true});
            });
        }
        return callback({success: true});
    });
};

MainController.prototype.getTagList = function (callback) {
    var me = this;
    me.tagModel.find({}, function (err, tags) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        return callback({ success: true, tags: tags});
    });
};

MainController.prototype.registerUser = function (login, image, tags, callback ) {
    var userModel = new this.userModel;
    userModel.login = login;
    userModel.image = image;
    var newTags = JSON.parse(tags);
    userModel.tags = newTags;
    userModel.save(function (err) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        return callback({success: true});
    });
};

MainController.prototype.logInPlace = function (user, placeName, placeId, callback) {
    /*
    var me = this;
    console.log(user);
    me.placeModel.findOne({_id : placeId}, function (err, place) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        if (place) {
            place.users.push(user);
            place.markModified("users");
            place.save(function (err) {
                if (err) {
                    return callback({success: false});
                }
                return callback({success: true});
            });
        } else {
            var newPlace = new me.placeModel({name: placeName, users : []});
            newPlace.users = [];
            newPlace.users.push(user);
            newPlace.markModified("users");
            newPlace.save(function (err) {
                if (err) {  
                    return callback({success: false});
                }
                return callback({success: true});
            });
        }

    });
    FUUUUU COS NIE DZIALA NO :(*/
    return callback({success: true});
};

MainController.prototype.getUser = function (login, callback) {
    var me = this;
    me.userModel.findOne({login:login}, function (err, user) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        if (user) {
            var newUser = {login: user.login, date: Date.now(), image: user.image};
            console.log(newUser);
            return callback({success: true, user: newUser});
        } else {
            return callback({success: false, msg: "Brak podanego uzytkownika"});
        }
    });
};

MainController.prototype.getUserTags = function (login, callback) {
    var me = this;
    me.userModel.findOne({login:login}, function (err, user) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        if (user) {
            return callback({success: true, tags: user.tags});
        } else {
            return callback({success: false, msg: "Brak podanego uzytkownika"});
        }
    });
};

MainController.prototype.getPlacesByCords = function () {

};

MainController.prototype.uploadImage = function (filename, path, login, callback) {
    var me = this;
    me.userModel.findOne( {login: login}, function (err, user) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        var superPath = "http://s150435.vm.wmi.amu.edu.pl:30000/api/images/" + login;
        user.image = superPath;
        user.save(function (err) {
            if (err) {
                return callback({success: false, msg: "Blad bazy danych"});
            }
            fs.readFile(path, function (err, data) {
                fs.writeFile("images/" + login, data, function (err) {
                    if (err) {
                        return callback({success: false, msg: "Blad bazy danych"});
                    } else {
                        return callback({success: true});
                    }
                });
            });
        });
    });
};

MainController.prototype.getPlacesByTags = function () {
    
};

MainController.prototype.isLoginAvailable = function (login, callback) {
    var me = this;
    me.userModel.count({login:login}, function (err, count) {
        if (err) {
            return callback({success: false, msg: "Blad bazy danych"});
        }
        if (count > 0) {
            return callback({success: true, available: false});
        } else {
            return callback({success: true, available: true});
        }
    });
};


module.exports = MainController;